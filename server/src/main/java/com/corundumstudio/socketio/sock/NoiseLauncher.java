package com.corundumstudio.socketio.sock;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;

import redis.clients.jedis.Jedis;

public class NoiseLauncher {
	
	public static int port = 9092;
	
	public static Jedis jedis = new Jedis("127.0.0.1", 6379);
	private static String redis_session_prefix = "tapgo";
	
    public static void main(String[] args) {

    	//kill port proccess (not work)
    	try {
        	Runtime rt = Runtime.getRuntime();
        	rt.exec("kill $(lsof -t -i:" + port + "|grep $(lsof -t -c java))");
		} catch (IOException e1) {}
    	
    	
        Configuration config = new Configuration();
        config.setHostname("localhost");
        config.setPort(port);

//        config.setKeyStorePassword("test1234");
//        InputStream stream = NoiseLauncher.class.getResourceAsStream("/keystore.jks");
//        config.setKeyStore(stream);
        
        final SocketIOServer server = new SocketIOServer(config);
        
        server.addConnectListener(new ConnectListener() {
			
			@Override
			public void onConnect(SocketIOClient client) {
				//get Cookie
				String PHPSESSID = "";
				try {
					String cookies = client.getHandshakeData().getHeaders().get("Cookie").get(0);
					Pattern pattern = Pattern.compile("PHPSESSID=([a-z0-9]{24,30})$");
			        Matcher matcher = pattern.matcher(cookies);
			        matcher.find();
		            PHPSESSID = matcher.group(1);	
				} catch (Exception e) {
					System.err.println("Can't find client PHPSESSID, " + e.getMessage());
					client.disconnect();
				}
				
				//get Session from Redis

				String session = "";
				try {
			        session = jedis.get(redis_session_prefix + PHPSESSID);	
				} catch (Exception e) {
				}
				
				//Check Auth
				if(session == "" || session == null){
					System.err.println("Can't find PHPSESSID in Redis, PHPSESSID=" + PHPSESSID);
					client.disconnect();
				}
		        //jedis.close();
		        
		        System.out.println("send history");
                server.getBroadcastOperations().sendEvent("history", getHistory(new Date()));
			}
		});
        
        //when client connect，send history 
//        server.addEventListener("history", NoiseObject.class, new DataListener<NoiseObject>() {
//			public void onData(SocketIOClient client, NoiseObject data, AckRequest ackSender) throws Exception {
//	        	System.out.println("send history");
//	        	
//                server.getBroadcastOperations().sendEvent("history", getHistory(data.getTime()));
//            }
//        });
        
        try {
            server.start();
		} catch (Exception e) {
			System.err.println("Address port " + port + " already in use, wait 10 sec and try again.");
			System.exit(0);
		}
        System.out.println("socket start");
        

        //real-time event, every second
        while(true){
        	ArrayList<NoiseObject> arr = getData();
        	if(arr.size() > 0){
        		server.getBroadcastOperations().sendEvent("noise_row", arr);
        	}
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        
		//FIXME:如何永久跑
        //FIXME:掛掉自動重啟
        
//        Thread.sleep(Integer.MAX_VALUE);

//        server.stop();
    }

    /**
     * 取得當下資料
     * @return ArrayList<NoiseObject> Noise Array
     */
    public static ArrayList<NoiseObject> getData() {
    	
        NoiseObject noise = new NoiseObject();
        noise.setTime(new Date());
        int random = 50 + (int)(Math.random() * 25); 
        noise.setNoise(new Double(random));

    	ArrayList<NoiseObject> arr = new ArrayList<NoiseObject>();
        arr.add(noise);
        
        return arr;
    }

    /**
     * 取得歷史資料
     * @param dataDate client當下時間
     * @return ArrayList<NoiseObject> Noise Array
     */
    public static ArrayList<NoiseObject> getHistory(Date dataDate) {
    	
    	//假資料
    	Calendar cal = Calendar.getInstance(Locale.TAIWAN);
    	cal.setTime(dataDate);
    	cal.add(Calendar.SECOND, -150);

    	ArrayList<NoiseObject> arr = new ArrayList<NoiseObject>();
    	for(int i = 1; i <= 150; i++){
    		cal.add(Calendar.SECOND, 1);
    		
    		NoiseObject noise = new NoiseObject();
            noise.setTime(cal.getTime());
            int random = 50 + (int)(Math.random() * 25); 
            noise.setNoise(new Double(random));
            arr.add(noise);
    	}
    	
        return arr;
    }
}