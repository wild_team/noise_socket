package com.corundumstudio.socketio.sock;

import java.util.Date;

public class NoiseObject {

    private Date time;
    private Double noise;

    public NoiseObject() {
    }

    public NoiseObject(Date time, Double noise) {
        super();
        this.time = time;
        this.noise = noise;
    }

    public Date getTime() {
        return time;
    }
    public void setTime(Date time) {
        this.time = time;
    }

    public Double getNoise() {
        return noise;
    }
    public void setNoise(Double noise) {
        this.noise = noise;
    }

}
